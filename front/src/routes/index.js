import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Home from '@/pages/home';
import Signup from '@/pages/signup';
import Profile from '@/pages/profile';
import User from '@/pages/user';
import Post from '@/pages/post';
import Hashtag from '@/pages/hashtag';
import AppLayout from '@/components/AppLayout';

const RouteWithLayout = ({ component: Component, layout: Layout, ...rest }) => (
	<Layout>
		<Route {...rest} render={routeProps => <Component {...routeProps} />} />
	</Layout>
);

const Routes = () => (
	<Switch>
		<RouteWithLayout exact path="/" component={Home} layout={AppLayout} />
		<RouteWithLayout path="/signup" component={Signup} layout={AppLayout} />
		<RouteWithLayout path="/profile" component={Profile} layout={AppLayout} />
		<RouteWithLayout
			exact
			path="/user/:id"
			component={User}
			layout={AppLayout}
		/>
		<RouteWithLayout
			exact
			path="/post/:id"
			component={Post}
			layout={AppLayout}
		/>
		<RouteWithLayout
			path="/hashtag/:tag"
			component={Hashtag}
			layout={AppLayout}
		/>
		<Redirect from="*" to="/" />
	</Switch>
);

export default Routes;
