import produce from 'immer';
import instance from '@/utils/axios';

const createUserSlice = set => ({
	user: {
		me: null,
		userInfo: null, // 유저 정보
		loadMyInfoLoading: false, // 로그인 정보 조회
		loadMyInfoDone: false,
		loadMyInfoError: null,
		loadUserLoading: false, // 유저 정보 조회
		loadUserDone: false,
		loadUserError: null,
		loginLoading: false, // 로그인 시도중
		loginDone: false,
		loginError: null,
		logoutLoading: false, // 로그아웃 시도중
		logoutDone: false,
		logoutError: null,
		signupLoading: false, // 회원가입 시도중
		signupDone: false,
		signupError: null,
		changeNicknameLoading: false, // 닉네임 변경 시도중
		changeNicknameDone: false,
		changeNicknameError: null,
		followLoading: false, // 팔로우
		followDone: false,
		followError: null,
		unfollowLoading: false, // 언팔로우 시도중
		unfollowDone: false,
		unfollowError: null,
		loadFollowingsLoading: false,
		loadFollowingsDone: false,
		loadFollowingsError: null,
		loadFollowersLoading: false,
		loadFollowersDone: false,
		loadFollowersError: null,
		login: async ({ email, password }) => {
			try {
				set(
					produce(state => {
						state.user.loginLoading = true;
						state.user.loginDone = false;
						state.user.loginError = null;
					}),
				);
				const response = await instance.post('/user/login', {
					email,
					password,
				});
				set(
					produce(state => {
						state.user.loginLoading = false;
						state.user.me = response.data;
						state.user.loginDone = true;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.loginLoading = false;
						state.user.loginError = error.response.data;
					}),
				);
			}
		},
		logout: async () => {
			try {
				set(
					produce(state => {
						state.user.logoutLoading = true;
						state.user.logoutDone = false;
						state.user.logoutError = null;
					}),
				);
				await instance.post('/user/logout');
				set(
					produce(state => {
						state.user.logoutLoading = false;
						state.user.logoutDone = true;
						state.user.me = null;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.logoutLoading = false;
						state.user.logoutError = error.response.data;
					}),
				);
			}
		},
		signup: async ({ email, nickname, password }) => {
			try {
				set(
					produce(state => {
						state.user.signupLoading = true;
						state.user.signupDone = false;
						state.user.signupError = null;
					}),
				);
				await instance.post('/user', {
					email,
					nickname,
					password,
				});
				set(
					produce(state => {
						state.user.signupLoading = false;
						state.user.signupDone = true;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.signupLoading = false;
						state.user.signupError = error.response.data;
					}),
				);
			}
		},
		loadMyInfo: async () => {
			try {
				set(
					produce(state => {
						state.user.loadMyInfoLoading = true;
						state.user.loadMyInfoDone = false;
						state.user.loadMyInfoError = null;
					}),
				);
				const response = await instance.get('/user');
				set(
					produce(state => {
						state.user.loadMyInfoLoading = false;
						state.user.loadMyInfoDone = true;
						state.user.me = response.data;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.loadMyInfoLoading = false;
						state.user.loadMyInfoError = error.response.data;
					}),
				);
			}
		},
		loadUser: async ({ userId }) => {
			try {
				set(
					produce(state => {
						state.user.loadUserLoading = true;
						state.user.loadUserDone = false;
						state.user.loadUserError = null;
					}),
				);
				const response = await instance.get(`/user/${userId}`);
				set(
					produce(state => {
						state.user.loadUserLoading = false;
						state.user.loadUserDone = true;
						state.user.userInfo = response.data;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.loadUserLoading = false;
						state.user.loadUserError = error.response.data;
					}),
				);
			}
		},
		changeNickname: async ({ nickname }) => {
			try {
				set(
					produce(state => {
						state.user.changeNicknameLoading = true;
						state.user.changeNicknameDone = false;
						state.user.changeNicknameError = null;
					}),
				);
				const response = await instance.patch('/user/nickname', { nickname });
				set(
					produce(state => {
						state.user.changeNicknameLoading = false;
						state.user.changeNicknameDone = true;
						state.user.me.nickname = response.data.nickname;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.changeNicknameLoading = false;
						state.user.changeNicknameError = error.response.data;
					}),
				);
			}
		},
		follow: async ({ userId }) => {
			try {
				set(
					produce(state => {
						state.user.followLoading = true;
						state.user.followDone = false;
						state.user.followError = null;
					}),
				);
				const response = await instance.patch(`/user/${userId}/follow`);
				console.log(response);
				set(
					produce(state => {
						state.user.followLoading = false;
						state.user.followDone = true;
						state.user.me.Followings.push({
							id: response.data.UserId,
						});
					}),
				);
			} catch (error) {
				console.log(error);
				set(
					produce(state => {
						state.user.followLoading = false;
						state.user.followError = error.response.data;
					}),
				);
			}
		},
		unfollow: async ({ userId }) => {
			try {
				set(
					produce(state => {
						state.user.unfollowLoading = true;
						state.user.unfollowDone = false;
						state.user.unfollowError = null;
					}),
				);
				const response = await instance.delete(`/user/${userId}/follow`);
				set(
					produce(state => {
						state.user.unfollowLoading = false;
						state.user.unfollowDone = true;
						state.user.me.Followings = state.user.me.Followings.filter(
							v => v.id !== response.data.UserId,
						);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.unfollowLoading = false;
						state.user.unfollowError = error.response.data;
					}),
				);
			}
		},
		removeFollow: async ({ userId }) => {
			try {
				set(
					produce(state => {
						state.user.followLoading = true;
						state.user.followDone = false;
						state.user.followError = null;
					}),
				);
				const response = await instance.delete(`/user/${userId}/follower`);
				set(
					produce(state => {
						state.user.followLoading = false;
						state.user.followDone = true;
						state.user.me.Followers = state.user.me.Followers.filter(
							v => v.id !== response.data.UserId,
						);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.followLoading = false;
						state.user.followError = error.response.data;
					}),
				);
			}
		},
		loadFollowers: async ({ followersLimit }) => {
			try {
				set(
					produce(state => {
						state.user.loadFollowersLoading = true;
						state.user.loadFollowersError = null;
						state.user.loadFollowersDone = false;
					}),
				);
				const response = await instance.get(
					`/user/followers?limit=${followersLimit}`,
				);
				set(
					produce(state => {
						state.user.loadFollowersLoading = false;
						state.user.loadFollowersDone = true;
						state.user.me.Followers = response.data;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.loadFollowersLoading = false;
						state.user.loadFollowersError = error;
					}),
				);
			}
		},
		loadFollowings: async ({ followingsLimit }) => {
			try {
				set(
					produce(state => {
						state.user.loadFollowingsLoading = true;
						state.user.loadFollowingsError = null;
						state.user.loadFollowingsDone = false;
					}),
				);
				const response = await instance.get(
					`/user/followings?limit=${followingsLimit}`,
				);
				set(
					produce(state => {
						state.user.loadFollowingsLoading = false;
						state.user.loadFollowingsDone = true;
						state.user.me.Followings = response.data;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.user.loadFollowingsLoading = false;
						state.user.loadFollowingsError = error;
					}),
				);
			}
		},
	},
});

export default createUserSlice;
