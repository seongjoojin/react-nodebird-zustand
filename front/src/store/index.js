import create from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import createPostSlice from './createPostSlice';
import createUserSlice from './createUserSlice';

let useStore = set => ({
	...createUserSlice(set),
	...createPostSlice(set),
});

// useStore = persist(useStore, {
// 	name: 'zustand-storage',
// });

useStore = devtools(useStore);

export default useStore = create(useStore);
