import produce from 'immer';

import instance from '@/utils/axios';

const createPostSlice = set => ({
	post: {
		mainPosts: [],
		hasMorePosts: true, // 다음 posts 여부
		singlePost: null,
		imagePaths: [],
		loadPostsLoading: false,
		loadPostsDone: false,
		loadPostsError: null,
		addPostLoading: false,
		addPostDone: false,
		addPostError: null,
		updatePostLoading: false,
		updatePostDone: false,
		updatePostError: null,
		removePostLoading: false,
		removePostDone: false,
		removePostError: null,
		addCommentLoading: false,
		addCommentDone: false,
		addCommentError: null,
		likePostLoading: false,
		likePostDone: false,
		likePostError: null,
		uploadImagesLoading: false,
		uploadImagesDone: false,
		uploadImagesError: null,
		retweetLoading: false,
		retweetDone: false,
		retweetError: null,
		removeImage: index => {
			set(
				produce(state => {
					state.post.imagePaths = state.post.imagePaths.filter(
						(v, i) => i !== index,
					);
				}),
			);
		},
		loadPosts: async lastId => {
			try {
				set(
					produce(state => {
						state.post.loadPostsLoading = true;
						state.post.loadPostsDone = false;
						state.post.loadPostsError = null;
					}),
				);
				const response = await instance.get(`/posts?last=${lastId || 0}`);
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsDone = true;
						state.post.mainPosts = lastId
							? state.post.mainPosts.concat(response.data)
							: response.data;
						state.post.hasMorePosts = response.data.length === 10;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsError = error.response.data;
					}),
				);
			}
		},
		loadHashtagPosts: async ({ lastId, hashtag }) => {
			try {
				set(
					produce(state => {
						state.post.loadPostsLoading = true;
						state.post.loadPostsDone = false;
						state.post.loadPostsError = null;
					}),
				);
				const response = await instance.get(
					`/hashtag/${encodeURIComponent(hashtag)}?last=${lastId || 0}`,
				);
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsDone = true;
						state.post.mainPosts = lastId
							? state.post.mainPosts.concat(response.data)
							: response.data;
						state.post.hasMorePosts = response.data.length === 10;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsError = error.response.data;
					}),
				);
			}
		},
		loadUserPosts: async ({ lastId, userId }) => {
			try {
				set(
					produce(state => {
						state.post.loadPostsLoading = true;
						state.post.loadPostsDone = false;
						state.post.loadPostsError = null;
					}),
				);
				const response = await instance.get(
					`/user/${userId}/posts?last=${lastId || 0}`,
				);
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsDone = true;
						state.post.mainPosts = lastId
							? state.post.mainPosts.concat(response.data)
							: response.data;
						state.post.hasMorePosts = response.data.length === 10;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsError = error.response.data;
					}),
				);
			}
		},
		addPost: async formData => {
			try {
				set(
					produce(state => {
						state.post.addPostLoading = true;
						state.post.addPostDone = false;
						state.post.addPostError = null;
					}),
				);
				const response = await instance.post('/post', formData);
				set(
					produce(state => {
						state.user.me.Posts.unshift({ id: response.data.id });
						state.post.addPostLoading = false;
						state.post.addPostDone = true;
						state.post.mainPosts.unshift(response.data);
						state.post.imagePaths = [];
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.addPostLoading = false;
						state.post.addPostError = error;
					}),
				);
			}
		},
		uploadImages: async imageFormData => {
			try {
				set(
					produce(state => {
						state.post.uploadImagesLoading = true;
						state.post.uploadImagesDone = false;
						state.post.uploadImagesError = null;
					}),
				);
				const response = await instance.post('/post/images', imageFormData);
				console.log(response.data);
				set(
					produce(state => {
						state.post.imagePaths = state.post.imagePaths.concat(response.data);
						state.post.uploadImagesLoading = false;
						state.post.uploadImagesDone = true;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.uploadImagesLoading = false;
						state.post.uploadImagesError = error.response.data;
					}),
				);
			}
		},
		addComment: async data => {
			try {
				set(
					produce(state => {
						state.post.addCommentLoading = true;
						state.post.addCommentDone = false;
						state.post.addCommentError = null;
					}),
				);
				const response = await instance.post(
					`/post/${data.postId}/comment`,
					data,
				);
				set(
					produce(state => {
						const post = state.post.mainPosts.find(
							v => v.id === response.data.PostId,
						);
						state.post.addCommentLoading = false;
						state.post.addCommentDone = true;
						post.Comments.unshift(response.data);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.addCommentLoading = false;
						state.post.addCommentError = error.response.data;
					}),
				);
			}
		},
		removePost: async ({ postId }) => {
			try {
				set(
					produce(state => {
						state.post.removePostLoading = true;
						state.post.removePostDone = false;
						state.post.removePostError = null;
					}),
				);
				const response = await instance.delete(`/post/${postId}`);
				set(
					produce(state => {
						state.user.me.Posts = state.user.me.Posts.filter(
							v => v.id !== postId,
						);
						state.post.removePostLoading = false;
						state.post.removePostDone = true;
						state.post.mainPosts = state.post.mainPosts.filter(
							v => v.id !== response.data.PostId,
						);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.removePostLoading = false;
						state.post.removePostError = error.response.data;
					}),
				);
			}
		},
		likePost: async ({ postId }) => {
			try {
				set(
					produce(state => {
						state.post.likePostLoading = true;
						state.post.likePostDone = false;
						state.post.likePostError = null;
					}),
				);
				const response = await instance.patch(`/post/${postId}/like`);
				set(
					produce(state => {
						const post = state.post.mainPosts.find(
							v => v.id === response.data.PostId,
						);
						state.post.likePostLoading = false;
						state.post.likePostDone = true;
						post.Likers.push({ id: response.data.UserId });
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.likePostLoading = false;
						state.post.likePostError = error.response.data;
					}),
				);
			}
		},
		unlikePost: async ({ postId }) => {
			try {
				set(
					produce(state => {
						state.post.likePostLoading = true;
						state.post.likePostDone = false;
						state.post.likePostError = null;
					}),
				);
				const response = await instance.delete(`/post/${postId}/like`);
				set(
					produce(state => {
						const post = state.post.mainPosts.find(
							v => v.id === response.data.PostId,
						);
						state.post.likePostLoading = false;
						state.post.likePostDone = true;
						post.Likers = post.Likers.filter(
							v => v.id !== response.data.UserId,
						);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.likePostLoading = false;
						state.post.likePostError = error.response.data;
					}),
				);
			}
		},
		retweet: async data => {
			try {
				set(
					produce(state => {
						state.post.retweetLoading = true;
						state.post.retweetDone = false;
					}),
				);
				const response = await instance.post(
					`/post/${data.postId}/retweet`,
					data,
				);
				set(
					produce(state => {
						state.post.retweetDone = true;
						state.post.retweetLoading = false;
						state.post.mainPosts.unshift(response.data);
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.retweetLoading = true;
						state.post.retweetError = error.response.data;
					}),
				);
			}
		},
		updatePost: async data => {
			try {
				set(
					produce(state => {
						state.post.updatePostLoading = true;
						state.post.updatePostDone = false;
						state.post.updatePostError = null;
					}),
				);
				const response = await instance.post(`/post/${data.postId}`, data);
				set(
					produce(state => {
						const post = state.post.mainPosts.find(
							v => v.id === response.data.PostId,
						);
						state.post.updatePostLoading = false;
						state.post.updatePostDone = true;
						post.content = response.data.content;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.updatePostLoading = false;
						state.post.updatePostError = error.response.data;
					}),
				);
			}
		},
		loadPost: async ({ postId }) => {
			try {
				set(
					produce(state => {
						state.post.loadPostsLoading = true;
						state.post.loadPostsDone = false;
						state.post.loadPostsError = null;
					}),
				);
				const response = await instance.get(`/post/${postId}`);
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsDone = true;
						state.post.singlePost = response.data;
					}),
				);
			} catch (error) {
				set(
					produce(state => {
						state.post.loadPostsLoading = false;
						state.post.loadPostsError = error.response.data;
					}),
				);
			}
		},
	},
});

export default createPostSlice;
