import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input } from 'formik-antd';
import { Formik } from 'formik';
import { Button, message } from 'antd';
import * as Yup from 'yup';
import { EditOutlined, UndoOutlined } from '@ant-design/icons';

import useStore from '@/store';

const PostCardContentSchema = Yup.object().shape({
	content: Yup.string()
		.min(5, '게시글은 5자 이상 입력하여 주십시오.')
		.required('게시글은 필수 입력 항목 입니다.'),
});

const PostCardContent = ({
	postId,
	postContent,
	editMode,
	onToggleChangePost,
}) => {
	const updatePost = useStore(state => state.post.updatePost);
	const updatePostLoading = useStore(state => state.post.updatePostLoading);
	const updatePostDone = useStore(state => state.post.updatePostDone);
	const updatePostError = useStore(state => state.post.updatePostError);

	const [action, setAction] = useState(null);
	useEffect(() => {
		if (action) {
			if (updatePostDone) {
				message.success('게시글이 수정되었습니다.');
			}
			if (updatePostError) {
				message.error(JSON.stringify(updatePostError, null, 4));
			}
			action.setSubmitting(false);
			action.resetForm();
			onToggleChangePost();
			setAction(null);
		}
	}, [updatePostDone, updatePostError]);
	return (
		<div>
			{editMode ? (
				<Formik
					initialValues={{ content: postContent }}
					validationSchema={PostCardContentSchema}
					onSubmit={(values, { setSubmitting, resetForm }) => {
						updatePost({
							postId,
							content: values.content,
						});
						setAction({ setSubmitting, resetForm });
					}}
				>
					<Form style={{ marginBottom: '20px' }}>
						<Form.Item name="content">
							<Input.TextArea
								name="content"
								maxLength={140}
								autoSize={{ minRows: 3, maxRows: 5 }}
								placeholder="어떤 신기한 일이 있었나요?"
							/>
						</Form.Item>
						<Button.Group>
							<Button htmlType="submit" loading={updatePostLoading}>
								<EditOutlined /> 수정
							</Button>
							<Button type="danger" onClick={onToggleChangePost}>
								<UndoOutlined /> 취소
							</Button>
						</Button.Group>
					</Form>
				</Formik>
			) : (
				postContent.split(/(#[^\s#]+)/g).map((v, i) => {
					if (v.match(/(#[^\s#]+)/g)) {
						return (
							<Link to={`/hashtag/${v.slice(1)}`} key={i}>
								{v}
							</Link>
						);
					}
					return v;
				})
			)}
		</div>
	);
};

PostCardContent.propTypes = {
	postId: PropTypes.number.isRequired,
	postContent: PropTypes.string.isRequired,
	editMode: PropTypes.bool,
	onToggleChangePost: PropTypes.func.isRequired,
};

PostCardContent.defaultProps = {
	editMode: false,
};

export default PostCardContent;
