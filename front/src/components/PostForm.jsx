import React, { useState, useRef, useCallback, useEffect } from 'react';
import { Formik } from 'formik';
import { Form, Input } from 'formik-antd';
import * as Yup from 'yup';
import { Button, Image, message, Space } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import useStore from '@/store';
import { imageUrl } from '@/config';

const PostSchema = Yup.object().shape({
	content: Yup.string()
		.min(3, '게시글은 3자 이상 입력하여 주십시오.')
		.required('게시글은 필수 입력 항목 입니다.'),
});

const PostForm = () => {
	const addPost = useStore(state => state.post.addPost);
	const uploadImages = useStore(state => state.post.uploadImages);
	const imagePaths = useStore(state => state.post.imagePaths);
	const addPostLoading = useStore(state => state.post.addPostLoading);
	const addPostDone = useStore(state => state.post.addPostDone);
	const addPostError = useStore(state => state.post.addPostError);
	const removeImage = useStore(state => state.post.removeImage);
	const me = useStore(state => state.user.me);
	const imageInput = useRef();
	const [action, setAction] = useState(null);
	const onClickImageUpload = useCallback(() => {
		imageInput.current.click();
	}, [imageInput.current]);
	const onChangeImages = useCallback(e => {
		console.log('images', e.target.files);
		const imageFormData = new FormData();
		[].forEach.call(e.target.files, f => {
			imageFormData.append('image', f);
		});
		uploadImages(imageFormData);
	}, []);
	const onRemoveImage = useCallback(index => {
		console.log('onRemoveImage', index);
		removeImage(index);
	}, []);
	useEffect(() => {
		if (action) {
			if (addPostDone) {
				message.success('게시글이 등록되었습니다.');
			}
			if (addPostError) {
				message.error(JSON.stringify(addPostError, null, 4));
			}
			action.setSubmitting(false);
			action.resetForm();
			setAction(null);
		}
	}, [addPostDone, addPostError]);
	return (
		<Formik
			initialValues={{ content: '' }}
			validationSchema={PostSchema}
			onSubmit={(values, { setSubmitting, resetForm }) => {
				const formData = new FormData();
				imagePaths.forEach(image => {
					formData.append('image', image);
				});
				formData.append('content', values.content);
				addPost(formData);
				setAction({ setSubmitting, resetForm });
			}}
		>
			<Form style={{ marginBottom: 45 }} encType="multipart/form-data">
				<Form.Item name="content">
					<Input.TextArea
						id="content"
						name="content"
						maxLength={140}
						autoSize={{ minRows: 3, maxRows: 5 }}
						placeholder="어떤 신기한 일이 있었나요?"
					/>
					<input
						type="file"
						name="image"
						multiple
						hidden
						ref={imageInput}
						onChange={onChangeImages}
					/>
				</Form.Item>
				<div style={{ position: 'relative', margin: 0 }}>
					<Button
						onClick={onClickImageUpload}
						style={{ position: 'absolute', right: 80, bottom: '-15px' }}
					>
						<UploadOutlined /> Images Upload
					</Button>
					<Button
						type="primary"
						htmlType="submit"
						loading={addPostLoading}
						style={{ position: 'absolute', right: 0, bottom: '-15px' }}
					>
						올리기
					</Button>
				</div>
				<Space size={8}>
					{imagePaths.map((v, i) => (
						<div style={{ margin: '5px 0 5px 0' }} key={v}>
							<img
								src={`http://localhost:3065/${v}`}
								style={{ width: '200px' }}
								alt={v}
							/>
							<div style={{ marginTop: '5px' }}>
								<Button type="danger" onClick={() => onRemoveImage(i)}>
									제거
								</Button>
							</div>
						</div>
					))}
				</Space>
			</Form>
		</Formik>
	);
};

export default PostForm;
