import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Avatar, Comment, List, Tooltip } from 'antd';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);

const CommentList = ({ post }) => (
	<List
		itemLayout="horizontal"
		dataSource={post.Comments}
		renderItem={item => (
			<li>
				<Comment
					author={item.User.nickname}
					avatar={
						<Link to={`/user/${item.User.id}`}>
							<Avatar>{item.User.nickname[0]}</Avatar>
						</Link>
					}
					content={item.content}
					datetime={
						<Tooltip title={dayjs(item.createdAt, 'YYYY-MM-DD HH:mm:ss')}>
							<span>{dayjs(item.createdAt).fromNow()}</span>
						</Tooltip>
					}
				/>
			</li>
		)}
	/>
);

CommentList.propTypes = {
	post: PropTypes.shape({
		id: PropTypes.number.isRequired,
		User: PropTypes.shape({
			id: PropTypes.number.isRequired,
			nickname: PropTypes.string.isRequired,
		}),
		content: PropTypes.string.isRequired,
		createdAt: PropTypes.string.isRequired,
		Comments: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				content: PropTypes.string.isRequired,
				User: PropTypes.shape({
					id: PropTypes.number.isRequired,
					nickname: PropTypes.string.isRequired,
				}),
			}),
		),
		Images: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				src: PropTypes.string.isRequired,
			}),
		),
		Likers: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
			}),
		),
	}).isRequired,
};

export default CommentList;
