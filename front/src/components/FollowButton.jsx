import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { UserAddOutlined, UserDeleteOutlined } from '@ant-design/icons';

import useStore from '@/store';

const FollowButton = ({ post }) => {
	const me = useStore(state => state.user.me);
	const followLoading = useStore(state => state.user.followLoading);
	const unfollowLoading = useStore(state => state.user.unfollowLoading);
	const follow = useStore(state => state.user.follow);
	const unfollow = useStore(state => state.user.unfollow);

	const isFollowing = me?.Followings.find(v => v.id === post.User.id);
	const [buttonType, setButtonType] = useState('primary');
	const [buttonIcon, setButtonIcon] = useState(<UserAddOutlined />);

	useEffect(() => {
		if (isFollowing) {
			// 팔로우
			setButtonType('');
			setButtonIcon(<UserDeleteOutlined />);
		} else {
			// 언팔로우
			setButtonType('primary');
			setButtonIcon(<UserAddOutlined />);
		}
	}, [isFollowing]);

	const onClickButton = useCallback(() => {
		if (buttonType === '') {
			// 언팔로우 요청
			unfollow({
				userId: post.User.id,
			});
			setButtonType('primary');
			setButtonIcon(<UserAddOutlined />);
		} else {
			// 팔로우 요청
			follow({
				userId: post.User.id,
			});
			setButtonType('');
			setButtonIcon(<UserDeleteOutlined />);
		}
	}, [buttonType]);

	return (
		<Button
			size="small"
			type={buttonType}
			icon={buttonIcon}
			onClick={onClickButton}
			loading={followLoading || unfollowLoading}
		>
			{isFollowing ? '팔로잉' : '팔로우'}
		</Button>
	);
};

FollowButton.propTypes = {
	post: PropTypes.shape({
		id: PropTypes.number.isRequired,
		User: PropTypes.shape({
			id: PropTypes.number.isRequired,
			nickname: PropTypes.string.isRequired,
		}),
		content: PropTypes.string.isRequired,
		createdAt: PropTypes.string.isRequired,
		Comments: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				content: PropTypes.string.isRequired,
			}),
		),
		Images: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				src: PropTypes.string.isRequired,
			}),
		),
		Likers: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
			}),
		),
	}).isRequired,
};
export default FollowButton;
