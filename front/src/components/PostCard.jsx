import React, { useState, useCallback, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
	Avatar,
	Button,
	Card,
	Divider,
	message,
	Popconfirm,
	Popover,
	Tooltip,
} from 'antd';
import {
	AlertOutlined,
	DeleteOutlined,
	EditOutlined,
	EllipsisOutlined,
	HeartOutlined,
	HeartTwoTone,
	MessageOutlined,
	RetweetOutlined,
	ZoomInOutlined,
} from '@ant-design/icons';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);

import useStore from '@/store';
import PostImages from './PostImages';
import CommentForm from './CommentForm';
import CommentList from './CommentList';
import PostCardContent from './PostCardContent';
import FollowButton from './FollowButton';

const PostCard = ({ post }) => {
	const me = useStore(state => state.user.me);
	const removePostLoading = useStore(state => state.post.removePostLoading);
	const retweetError = useStore(state => state.post.retweetError);
	const likePost = useStore(state => state.post.likePost);
	const removePost = useStore(state => state.post.removePost);
	const retweet = useStore(state => state.post.retweet);
	const unlikePost = useStore(state => state.post.unlikePost);

	const history = useHistory();

	const [commentFormOpened, setCommentFormOpened] = useState(false);
	const [editMode, setEditMode] = useState(false);

	const onToggleChangePost = useCallback(() => {
		setEditMode(prev => !prev);
	}, []);
	const onToggleComment = useCallback(() => {
		setCommentFormOpened(prev => !prev);
	}, []);

	const onZoomPost = useCallback(() => {
		history.push(`/post/${post.id}`);
	}, []);

	const onLikePost = useCallback(() => {
		if (!me?.id) {
			message.warn('로그인이 필요합니다.').then();
			return;
		}
		likePost({
			postId: post.id,
		});
	}, [me?.id]);

	const onUnlikePost = useCallback(() => {
		if (!me?.id) {
			message.warn('로그인이 필요합니다.').then();
			return;
		}
		unlikePost({
			postId: post.id,
		});
	}, [me?.id]);

	const onRemovePost = useCallback(() => {
		if (!me?.id) {
			message.warn('로그인이 필요합니다.').then();
			return;
		}
		removePost({
			postId: post.id,
		});
	}, [me?.id]);

	const onRetweet = useCallback(() => {
		if (!me?.id) {
			message.warn('로그인이 필요합니다.').then();
			return;
		}
		retweet({
			postId: post.id,
		});
	}, [me?.id]);

	useEffect(() => {
		if (retweetError) {
			message.error(JSON.stringify(retweetError, null, 4)).then();
		}
	}, [retweetError]);

	const liked = post.Likers.find(v => v.id === me?.id);

	return (
		<div style={{ marginBottom: 20 }}>
			<Card
				cover={
					post.Images[0] && (
						<PostImages id={post.User.id} images={post.Images} />
					)
				}
				hoverable
				actions={[
					<ZoomInOutlined key="zoom" title="자세히" onClick={onZoomPost} />,
					<RetweetOutlined key="retweet" title="리트윗" onClick={onRetweet} />,
					liked ? (
						<HeartTwoTone
							twoToneColor="#eb2f96"
							title="좋아요"
							key="heart"
							onClick={onUnlikePost}
						/>
					) : (
						<HeartOutlined key="heart" title="좋아요" onClick={onLikePost} />
					),
					<MessageOutlined
						key="message"
						title="댓글"
						onClick={onToggleComment}
					/>,
					<Popover
						key="more"
						content={
							<Button.Group>
								{me?.id && post.User.id === me?.id ? (
									<>
										{!post.RetweetId && (
											<Button onClick={onToggleChangePost}>
												<EditOutlined /> 수정
											</Button>
										)}
										<Popconfirm
											title="Are you sure delete this task?"
											okText="Yes"
											onConfirm={onRemovePost}
											cancelText="No"
										>
											<Button type="danger" loading={removePostLoading}>
												<DeleteOutlined /> 삭제
											</Button>
										</Popconfirm>
									</>
								) : (
									<Button>
										<AlertOutlined /> 신고
									</Button>
								)}
							</Button.Group>
						}
					>
						<EllipsisOutlined />
					</Popover>,
				]}
				title={
					post.RetweetId ? `${post.User.nickname}님이 리트윗 하셨습니다.` : null
				}
				extra={
					me?.id && post.User.id !== me?.id && <FollowButton post={post} />
				}
			>
				{post.RetweetId && post.Retweet ? (
					<Card
						cover={
							post.Retweet.Images[0] && (
								<PostImages images={post.Retweet.Images} />
							)
						}
					>
						<Card.Meta
							avatar={
								<Link to={`/user/${post.Retweet.User.id}`}>
									<Avatar>{post.Retweet.User.nickname[0]}</Avatar>
								</Link>
							}
							title={
								<div>
									{post.Retweet.User.nickname}
									<Tooltip
										title={dayjs(post.Retweet.createdAt).format(
											'YYYY-MM-DD HH:mm:ss',
										)}
									>
										<span
											style={{
												color: '#ccc',
												marginLeft: '10px',
												fontSize: '14px',
											}}
										>
											{dayjs(post.Retweet.createdAt).fromNow()}
										</span>
									</Tooltip>
								</div>
							}
							description={
								<PostCardContent
									postId={post.RetweetId}
									postContent={post.Retweet.content}
									onToggleChangePost={onToggleChangePost}
								/>
							}
						/>
					</Card>
				) : (
					<Card.Meta
						avatar={
							<Link to={`/user/${post.User.id}`}>
								<Avatar>{post.User.nickname[0]}</Avatar>
							</Link>
						}
						title={
							<div>
								{post.User.nickname}
								<Tooltip
									title={dayjs(post.createdAt).format('YYYY-MM-DD HH:mm:ss')}
								>
									<span
										style={{
											color: '#ccc',
											marginLeft: '10px',
											fontSize: '14px',
										}}
									>
										{dayjs(post.createdAt).fromNow()}
									</span>
								</Tooltip>
							</div>
						}
						description={
							<PostCardContent
								postId={post.id}
								postContent={post.content}
								editMode={editMode}
								onToggleChangePost={onToggleChangePost}
							/>
						}
					/>
				)}
				{commentFormOpened && (
					<div>
						<Divider plain>{`${post.Comments.length}개의 댓글`}</Divider>
						<CommentList post={post} />
						{me?.id && <CommentForm post={post} />}
					</div>
				)}
			</Card>
		</div>
	);
};

PostCard.propTypes = {
	post: PropTypes.shape({
		id: PropTypes.number.isRequired,
		User: PropTypes.shape({
			id: PropTypes.number.isRequired,
			nickname: PropTypes.string.isRequired,
		}),
		content: PropTypes.string.isRequired,
		createdAt: PropTypes.string.isRequired,
		Comments: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				content: PropTypes.string.isRequired,
			}),
		),
		Images: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
				src: PropTypes.string.isRequired,
			}),
		),
		Likers: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number.isRequired,
			}),
		),
		RetweetId: PropTypes.number,
		Retweet: PropTypes.objectOf(PropTypes.any),
	}).isRequired,
};

export default PostCard;
