import React, { useCallback, useEffect, useState } from 'react';
import { Form, Input, message } from 'antd';
import styled from '@emotion/styled';

import useStore from '@/store';

const NicknameEditForm = () => {
	const me = useStore(state => state.user.me);
	const changeNickname = useStore(state => state.user.changeNickname);
	const changeNicknameLoading = useStore(
		state => state.user.changeNicknameLoading,
	);
	const changeNicknameDone = useStore(state => state.user.changeNicknameDone);
	const changeNicknameError = useStore(state => state.user.changeNicknameError);
	const [nickname, setNickname] = useState('');
	const onChangeNickname = useCallback(
		e => {
			setNickname(e.target.value);
		},
		[nickname],
	);
	const onSubmit = useCallback(() => {
		changeNickname({ nickname });
	}, [nickname]);

	useEffect(() => {
		if (nickname) {
			if (changeNicknameDone) {
				message.success('닉네임이 변경 되었습니다.');
			}
			if (changeNicknameError) {
				message.error(JSON.stringify(changeNicknameError, null, 4));
			}
		}
	}, [changeNicknameDone, changeNicknameError]);

	return (
		<FormWrapper>
			<Form.Item name="nickname">
				<Input.Search
					name="nickname"
					placeholder={me.nickname}
					addonBefore="닉네임"
					enterButton="수정"
					value={nickname}
					onChange={onChangeNickname}
					onSearch={onSubmit}
					loading={changeNicknameLoading}
				/>
			</Form.Item>
		</FormWrapper>
	);
};

const FormWrapper = styled(Form)`
	margin-bottom: 20px;
	border: 1px solid #d9d9d9;
	padding: 20px;
	box-sizing: border-box;
`;

export default NicknameEditForm;
