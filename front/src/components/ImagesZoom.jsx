import React, { useState } from 'react';
import { Global, css } from '@emotion/react';
import styled from '@emotion/styled';
import { CloseOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import Slick from 'react-slick';

import { imageUrl } from '@/config';

const ImagesZoom = ({ id, images, onClose }) => {
	const [currentSlide, setCurrentSlide] = useState(0);
	return (
		<Overlay>
			<Global
				styles={css`
					.slick-slide {
						display: inline-block;
					}

					.ant-card-cover {
						transform: none !important;
					}
				`}
			/>
			<Header>
				<h1>상세 이미지</h1>
				<CloseBtn onClick={onClose}>X</CloseBtn>
			</Header>
			<SlickWrapper>
				<div>
					<Slick
						initialSlide={0}
						afterChange={slide => setCurrentSlide(slide)}
						infinite
						arrows={false}
						slideToShow={1}
						slideToScroll={1}
					>
						{images.map(v => (
							<ImgWrapper key={v.src}>
								<img
									src={
										imageUrl
											? `${imageUrl}/${id}/${v.src}`
											: v.src.replace(/\/thumb\//, '/original/')
									}
									alt={v.src}
								/>
							</ImgWrapper>
						))}
					</Slick>
					<Indicator>
						<div>
							{currentSlide + 1}&nbsp;/&nbsp;
							{images.length}
						</div>
					</Indicator>
				</div>
			</SlickWrapper>
		</Overlay>
	);
};

export const Overlay = styled.div`
	position: fixed;
	z-index: 5000;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
`;

export const Header = styled.header`
	header: 44px;
	background: white;
	position: relative;
	padding: 0;
	text-align: center;

	& h1 {
		margin: 0;
		font-size: 17px;
		color: #333;
		line-height: 44px;
	}
`;

export const CloseBtn = styled(CloseOutlined)`
	position: absolute;
	right: 0;
	top: 0;
	padding: 15px;
	line-height: 14px;
	cursor: pointer;
`;

export const SlickWrapper = styled.div`
	height: calc(100% - 44px);
	background: #090909;
`;

export const ImgWrapper = styled.div`
	padding: 32px;
	text-align: center;

	& img {
		margin: 0 auto;
		max-height: 750px;
	}
`;

export const Indicator = styled.div`
	text-align: center;

	& > div {
		width: 75px;
		height: 30px;
		line-height: 30px;
		border-radius: 15px;
		background: #313131;
		display: inline-block;
		text-align: center;
		color: white;
		font-size: 15px;
	}
`;

ImagesZoom.propTypes = {
	id: PropTypes.number.isRequired,
	images: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number.isRequired,
			src: PropTypes.string.isRequired,
		}),
	).isRequired,
	onClose: PropTypes.func.isRequired,
};

export default ImagesZoom;
