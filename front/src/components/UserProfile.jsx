import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import { Avatar, Button, Card } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';

import useStore from '@/store';

const UserProfile = () => {
	const logout = useStore(state => state.user.logout);
	const me = useStore(state => state.user.me);
	const logoutLoading = useStore(state => state.user.logoutLoading);
	const onLogout = useCallback(() => {
		logout();
	}, []);
	return (
		<Card
			actions={[
				<div key="twit">
					<Link to={`/user/${me.id}`}>
						게시글
						<br />
						{me.Posts.length}
					</Link>
				</div>,
				<div key="followings">
					<Link to="/profile">
						팔로잉
						<br />
						{me.Followings.length}
					</Link>
				</div>,
				<div key="followers">
					<Link to="/profile">
						팔로워
						<br />
						{me.Followers.length}
					</Link>
				</div>,
			]}
		>
			<Card.Meta
				avatar={
					<Link to={`/user/${me.id}`}>
						<Avatar>{me.nickname[0]}</Avatar>
					</Link>
				}
				title={me.nickname}
			/>
			<Button
				style={{ marginTop: 15 }}
				onClick={onLogout}
				loading={logoutLoading}
			>
				<LogoutOutlined /> 로그아웃
			</Button>
		</Card>
	);
};

export default UserProfile;
