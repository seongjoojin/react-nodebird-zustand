import axios from 'axios';

import { backendUrl } from '@/config';

const instance = axios.create({
	baseURL: backendUrl,
	withCredentials: true,
});

instance.interceptors.request.use(
	config => {
		// 토큰을 쿠키 혹은 로컬스토리지에 담는 작업 하기
		// config.headers.Authorization = localStorage.getItem('token');
		return config;
	},
	error => {
		return Promise.reject(error);
	},
);

instance.interceptors.response.use(
	response => {
		return response;
	},
	error => {
		return Promise.reject(error);
	},
);

export default instance;
