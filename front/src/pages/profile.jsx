import React, { useState, useCallback, useEffect } from 'react';
import { message } from 'antd';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';

import useStore from '@/store';
import FollowList from '@/components/FollowList';
import NicknameEditForm from '@/components/NicknameEditForm';

const Profile = () => {
	const me = useStore(state => state.user.me);
	const loadMyInfo = useStore(state => state.user.loadMyInfo);
	const loadFollowers = useStore(state => state.user.loadFollowers);
	const loadFollowings = useStore(state => state.user.loadFollowings);
	const loadFollowingsError = useStore(state => state.user.loadFollowingsError);
	const loadFollowersError = useStore(state => state.user.loadFollowersError);
	const loadFollowersDone = useStore(state => state.user.loadFollowersDone);
	const loadFollowersLoading = useStore(
		state => state.user.loadFollowersLoading,
	);
	const loadFollowingsLoading = useStore(
		state => state.user.loadFollowingsLoading,
	);
	const loadFollowingsDone = useStore(state => state.user.loadFollowingsDone);

	const history = useHistory();
	const [followersLimit, setFollowersLimit] = useState(3);
	const [followingsLimit, setFollowingsLimit] = useState(3);

	const loadMoreFollowings = useCallback(() => {
		setFollowingsLimit(prev => prev + 3);
	}, []);
	const loadMoreFollowers = useCallback(() => {
		setFollowersLimit(prev => prev + 3);
	}, []);

	useEffect(() => {
		loadMyInfo().then(() => {
			if (!(me && me.id)) {
				message.warn('로그인 후 이용해 주시길 바랍니다.');
				history.push('/');
			}
		});
	}, []);

	useEffect(() => {
		loadFollowers({ followersLimit });
		loadFollowings({ followingsLimit });
	}, []);

	if (!me) {
		return null;
	}

	if (loadFollowingsError || loadFollowersError) {
		return <div>팔로잉/팔로워 로딩 중 에러가 발생합니다.</div>;
	}

	return (
		<>
			<Helmet>
				<title>내 프로필 | NodeBird</title>
			</Helmet>
			<NicknameEditForm />
			{loadFollowingsDone && (
				<FollowList
					header="팔로잉"
					data={me?.Followings}
					onClickMore={loadMoreFollowings}
					loading={loadFollowingsLoading}
				/>
			)}
			{loadFollowersDone && (
				<FollowList
					header="팔로워"
					data={me?.Followers}
					onClickMore={loadMoreFollowers}
					loading={loadFollowersLoading}
				/>
			)}
		</>
	);
};

export default Profile;
