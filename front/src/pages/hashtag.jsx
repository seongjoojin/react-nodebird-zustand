import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import useStore from '@/store';
import PostCard from '@/components/PostCard';

const Hashtag = () => {
	const { tag } = useParams();
	const loadMyInfo = useStore(state => state.user.loadMyInfo);
	const loadHashtagPosts = useStore(state => state.post.loadHashtagPosts);
	const mainPosts = useStore(state => state.post.mainPosts);
	const hasMorePosts = useStore(state => state.post.hasMorePosts);
	const loadPostsLoading = useStore(state => state.post.loadPostsLoading);

	useEffect(() => {
		loadMyInfo();
		loadHashtagPosts({ hashtag: tag });
	}, []);

	useEffect(() => {
		const onScroll = () => {
			if (hasMorePosts && !loadPostsLoading) {
				if (
					window.pageYOffset + document.documentElement.clientHeight >
					document.documentElement.scrollHeight - 300
				) {
					const lastId = mainPosts[mainPosts.length - 1]?.id;
					loadHashtagPosts({
						lastId,
						hashtag: tag,
					});
				}
			}
		};
		window.addEventListener('scroll', onScroll);
		return () => {
			window.removeEventListener('scroll', onScroll);
		};
	}, [mainPosts.length, hasMorePosts, tag, loadPostsLoading]);

	return (
		<>
			{mainPosts.map(c => (
				<PostCard key={c.id} post={c} />
			))}
		</>
	);
};

export default Hashtag;
