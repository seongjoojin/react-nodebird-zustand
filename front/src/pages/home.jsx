import React, { useEffect } from 'react';

import PostForm from '@/components/PostForm';
import PostCard from '@/components/PostCard';
import useStore from '@/store';

const Home = () => {
	const loadMyInfo = useStore(state => state.user.loadMyInfo);
	const loadPosts = useStore(state => state.post.loadPosts);
	const me = useStore(state => state.user.me);
	const mainPosts = useStore(state => state.post.mainPosts);
	const hasMorePosts = useStore(state => state.post.hasMorePosts);
	const loadPostsLoading = useStore(state => state.post.loadPostsLoading);
	useEffect(() => {
		loadMyInfo();
		loadPosts();
	}, []);
	useEffect(() => {
		function onScroll() {
			// window.scrollY : 얼마나 내렸는지
			// document.documentElement.clientHeight : 화면에 보이는 길이
			// document.documentElement.scrollHeight : 총길이
			if (hasMorePosts && !loadPostsLoading) {
				if (
					window.scrollY + document.documentElement.clientHeight >
					document.documentElement.scrollHeight - 300
				) {
					const lastId = mainPosts[mainPosts.length - 1]?.id;
					loadPosts({
						lastId,
					});
				}
			}
		}
		window.addEventListener('scroll', onScroll);
		return () => {
			window.removeEventListener('scroll', onScroll);
		};
	}, [hasMorePosts, loadPostsLoading, mainPosts]);
	return (
		<>
			{me && <PostForm />}
			{mainPosts.map(post => (
				<PostCard key={post.id} post={post} />
			))}
		</>
	);
};

export default Home;
