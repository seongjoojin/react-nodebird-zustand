import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import useStore from '@/store';
import PostCard from '@/components/PostCard';

const Post = () => {
	const { id } = useParams();
	const loadMyInfo = useStore(state => state.user.loadMyInfo);
	const loadPost = useStore(state => state.post.loadPost);
	const singlePost = useStore(state => state.post.singlePost);

	useEffect(() => {
		loadMyInfo();
		loadPost({ postId: id });
	}, []);

	return (
		<div>
			{singlePost && (
				<>
					<Helmet>
						<title>{singlePost.User.nickname} 님의 글</title>
					</Helmet>
					<PostCard post={singlePost} />
				</>
			)}
		</div>
	);
};

export default Post;
