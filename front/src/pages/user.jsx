import React, { useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Avatar, Card } from 'antd';

import useStore from '@/store';
import PostCard from '@/components/PostCard';

const User = () => {
	const loadUserPosts = useStore(state => state.post.loadUserPosts);
	const loadMyInfo = useStore(state => state.user.loadMyInfo);
	const loadUser = useStore(state => state.user.loadUser);
	const mainPosts = useStore(state => state.post.mainPosts);
	const hasMorePosts = useStore(state => state.post.hasMorePosts);
	const loadPostsLoading = useStore(state => state.post.loadPostsLoading);
	const userInfo = useStore(state => state.user.userInfo);
	const me = useStore(state => state.user.me);
	const { id } = useParams();

	useEffect(() => {
		loadMyInfo();
		loadUser({ userId: id });
		loadUserPosts({ userId: id });
	}, []);

	useEffect(() => {
		const onScroll = () => {
			if (hasMorePosts && !loadPostsLoading) {
				if (
					window.pageYOffset + document.documentElement.clientHeight >
					document.documentElement.scrollHeight - 300
				) {
					const lastId = mainPosts[mainPosts.length - 1]?.id;
					loadUserPosts({
						lastId,
						userId: id,
					});
				}
			}
		};
		window.addEventListener('scroll', onScroll);
		return () => {
			window.removeEventListener('scroll', onScroll);
		};
	}, [mainPosts.length, hasMorePosts, id, loadPostsLoading]);

	return (
		<>
			<Helmet>
				{userInfo && (
					<title>
						{userInfo.nickname}
						님의 글
					</title>
				)}
			</Helmet>
			{userInfo && userInfo.id !== me?.id ? (
				<div style={{ padding: 15, background: '#ececec', marginBottom: 20 }}>
					<Card
						actions={[
							<div key="twit">
								짹짹
								<br />
								{userInfo.Posts}
							</div>,
							<div key="following">
								팔로잉
								<br />
								{userInfo.Followings}
							</div>,
							<div key="follower">
								팔로워
								<br />
								{userInfo.Followers}
							</div>,
						]}
					>
						<Card.Meta
							avatar={
								<Link to={`/user/${userInfo.id}`}>
									<Avatar>{userInfo.nickname[0]}</Avatar>
								</Link>
							}
							title={userInfo.nickname}
						/>
					</Card>
				</div>
			) : null}
			{mainPosts.map(post => (
				<PostCard key={post.id} post={post} />
			))}
		</>
	);
};

export default User;
