const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
	devtool: 'inline-source-map',
	mode: 'development',
	resolve: {
		extensions: ['.js', '.jsx', '.json'],
		alias: {
			'@': path.resolve(__dirname, './src'),
		},
	},
	entry: { app: './src/index.js' },
	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
		filename: '[name].js',
		chunkFilename: '[name].js',
		clean: true,
	},
	module: {
		rules: [
			{
				test: /\.(jsx?)(\?.*)?$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.s?css$/,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
					},
				],
			},
			{
				test: /\.(png|jpe?g|gif|webp)(\?.*)?$/,
				type: 'asset',
			},
			{
				test: /\.(svg)(\?.*)?$/,
				type: 'asset',
			},
			{
				test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
				type: 'asset',
			},
			{
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				type: 'asset',
			},
		],
	},
	plugins: [
		new HtmlPlugin({
			template: './index.html',
			filename: 'index.html',
		}),
	],
	devServer: {
		host: 'localhost',
		port: 3090,
		hot: true,
		historyApiFallback: {
			disableDotRule: true,
		},
	},
};
